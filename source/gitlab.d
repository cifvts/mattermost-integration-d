import std.format;

import std.format;
import std.uni;

import vibe.d;
import vibe.data.json;

import requests;

class Gitlab {
    private Json _conf;

    this(Json conf) {
        try {
            _conf = conf.clone;
        }
        catch(Throwable) {
            logInfo("GitLab Configuration missing");
        }
    }

    int handler(Json data) {
        string text;
        string project_name;
        Json mm_text = Json.emptyObject;

        if (data["object_kind"] == "push") {
            project_name = data["project"]["name"].get!string;
            text = format_push(data);
        } else if (data["object_kind"] == "issue") {
            project_name = data["project"]["name"].get!string;
            text = format_issue(data);
        } else if (data["object_kind"] == "tag_push") {
            project_name = data["project"]["name"].get!string;
            text = format_tag(data);
        } else if (data["object_kind"] == "note") {
            project_name = data["project"]["name"].get!string;
            text = format_note(data);
        } else if (data["object_kind"] == "merge_request") {
            project_name = data["object_attributes"]["target"]["name"].get!string;
            text = format_merge(data);
        } else if (data["object_kind"] == "wiki_page") {
            project_name = data["project"]["name"].get!string;
            text = format_wiki(data);
        } else if (data["object_kind"] == "pipeline") {
            project_name = data["project"]["name"].get!string;
            text = format_pipeline(data);
        } else if (data["object_kind"] == "build") {
            project_name = data["repository"]["name"].get!string;
            text = format_build(data);
        }

        mm_text["text"] = text;
        try {
            mm_text["username"] =
                    this._conf[project_name]["username"].get!string;
        } catch(Throwable) {}
        try {
            mm_text["icon_url"] =
                    this._conf[project_name]["icon_url"].get!string;
        } catch(Throwable) {}

        Request mm_req = Request();
        try {
            auto mm_res = mm_req.post(
                    this._conf[project_name]["webhook_in"].get!string,
                    mm_text.toPrettyString(),
                    "application/json");
            return mm_res.code;
        } catch(Throwable) {
            logInfo(format("Project %s not configured, action was %s",
                        project_name, data["object_kind"].get!string));
            return 404;
        }
    }

    private string quote_string(string input) {
        auto output = "";
        foreach (line; splitLines(input)) {
            output ~= "> " ~ line ~ "\n";
        }

        return (output);
    }

    private string format_push(Json data) {
        return format(
                "%s pushed %d commit(s) into the `%s`"
                ~ " branch for project [%s](%s).",
                data["user_name"].get!string,
                data["total_commits_count"].get!int,
                data["ref"].get!string,
                data["project"]["name"].get!string,
                data["project"]["homepage"].get!string
                );
    }

    private string format_issue(Json data) {
        string state = data["object_attributes"]["state"].get!string;
        string description;

        if (state == "opened" || state == "reopened")
            description ~= data["object_attributes"]["description"].get!string;

        return format(
                "##### [%s](%s)\n*[Issue #%d](%s/issues)"
                ~ " created by [%s](%s/u/%s) in [%s](%s) on [%s](%s)*\n %s",
                data["object_attributes"]["title"].get!string,
                data["object_attributes"]["url"].get!string,
                data["object_attributes"]["iid"].get!int,
                data["project"]["homepage"].get!string,
                data["user"]["name"].get!string,
                _conf["gitlab_address"].get!string,
                data["user"]["username"].get!string,
                data["project"]["name"].get!string,
                data["project"]["homepage"].get!string,
                data["object_attributes"]["created_at"].get!string,
                data["object_attributes"]["url"].get!string,
                description
                );
    }

    private string format_tag(Json data) {
        return format(
                "%s pushed tag `%s` to the project [%s](%s).",
                data["user_name"].get!string,
                data["ref"].get!string,
                data["project"]["name"].get!string,
                data["project"]["homepage"].get!string
                );
    }

    private string format_note(Json data) {
        string symbol;
        string type_grammar = "a";
        string note_type =
                data["object_attributes"]["noteable_type"].get!string.toLower;
        int note_id;
        string parent_title;
        string subtitle;
        string description;

        if (note_type == "mergerequest") {
            symbol = "!";
            note_id = data["merge_request"]["iid"].get!int;
            parent_title = data["merge_request"]["title"].get!string;
            note_type = "merge request";
        } else if (note_type == "snippet") {
            symbol = "$";
            note_id = data["snippet"]["id"].get!int;
            parent_title = data["snippet"]["title"].get!string;
        } else if (note_type == "issue") {
            symbol = "#";
            note_id = data["issue"]["iid"].get!int;
            parent_title = data["issue"]["title"].get!string;
            type_grammar ~= "n";
        }

        if (note_type == "commit") {
            subtitle = data["commit"]["id"].get!string;
        } else {
            subtitle = format(
                    "%s%d - %s",
                    symbol,
                    note_id,
                    parent_title
                    );
        }

        description = quote_string(data["object_attributes"]["note"].get!string);

        return format(
                "##### **New Comment** on [%s](%s)\n"
                ~ "*[%s](%s/u/%s)"
                ~ " commented on %s %s in [%s](%s) on [%s](%s)*\n %s",
                subtitle,
                data["object_attributes"]["url"].get!string,
                data["user"]["name"].get!string,
                _conf["gitlab_address"].get!string,
                data["user"]["username"].get!string,
                type_grammar,
                note_type,
                data["project"]["name"].get!string,
                data["project"]["homepage"].get!string,
                data["object_attributes"]["created_at"].get!string,
                data["object_attributes"]["url"].get!string,
                description
                );
    }

    private string format_merge(Json data) {
        string state = data["object_attributes"]["state"].get!string;
        string text_action;

        if (state == "opened") {
            text_action = "created a";
        } else if (state == "reopened") {
            text_action = "reopened a";
        } else if (state == "updated") {
            text_action = "updated a";
        } else if (state == "merged") {
            text_action = "merged a";
        } else if (state == "closed") {
            text_action = "closed a";
        }

        return format(
                "##### [!%d - %s](%s)\n*[%s](%s/u/%s)"
                ~ " %s merge request in [%s](%s) on [%s](%s)*\n"
                ~ "%s -> %s",
                data["object_attributes"]["iid"].get!int,
                data["object_attributes"]["title"].get!string,
                data["object_attributes"]["url"].get!string,
                data["user"]["name"].get!string,
                _conf["gitlab_address"].get!string,
                data["user"]["username"].get!string,
                text_action,
                data["object_attributes"]["target"]["name"].get!string,
                data["object_attributes"]["target"]["web_url"].get!string,
                data["object_attributes"]["created_at"].get!string,
                data["object_attributes"]["url"].get!string,
                data["object_attributes"]["source_branch"].get!string,
                data["object_attributes"]["target_branch"].get!string
                );
    }

    private string format_wiki(Json data) {
        string action;

        if (data["object_attributes"]["action"] == "create")
            action = "created";
        else
            action = "updated";

        return format(
                "##### Wiki page [%s](%s) of project [%s](%s)"
                ~ " has been %s by [%s](%s/u/%s)",
                data["object_attributes"]["title"].get!string,
                data["object_attributes"]["url"].get!string,
                data["project"]["name"].get!string,
                data["project"]["web_url"].get!string,
                action,
                data["user"]["name"].get!string,
                _conf["gitlab_address"].get!string,
                data["user"]["username"].get!string);
    }

    private string format_pipeline(Json data) {
        return format(
                "##### Build for [%s](%s): %s\n"
                ~ "Started at: %s, End at: %s\n"
                ~ "%s\n",
                data["object_attributes"]["ref"].get!string,
                data["commit"]["url"],
                data["object_attributes"]["status"].get!string,
                data["object_attributes"]["created_at"].get!string,
                data["object_attributes"]["finished_at"].type == Json.Type.null_
                    ? "Ongoing"
                    : data["object_attributes"]["finished_at"].get!string,
                quote_string(data["commit"]["message"].get!string));
    }

    private string format_build(Json data) {
        return format(
                "##### Build for [%s](%s): %s: %s\n"
                ~ "Started at: %s, End at: %s, (%ds)\n"
                ~ "%s\n",
                data["repository"]["name"].get!string,
                data["repository"]["homepage"].get!string,
                data["build_name"].get!string,
                data["build_status"].get!string,
                data["build_started_at"].get!string,
                data["build_finished_at"].get!string,
                data["build_duration"],
                quote_string(data["commit"]["message"].get!string));
    }

}

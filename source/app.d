import std.file;

import vibe.d;
import vibe.data.json;
import vibe.core.args;

import gitlab;

Gitlab gl;

shared static this()
{
    Json conf = parseJsonString(cast(string)read("source/conf.json"));

    gl = new Gitlab(conf["gitlab"]);

    auto settings = new HTTPServerSettings;
    settings.port = conf["port"].get!short;
    settings.bindAddresses = [conf["bind_ip"].get!string];

    auto router = new URLRouter;
    router
        .post("/gitlab_event", &gitlab_event)
        .post("*", &def);

    listenHTTP(settings, router);

    logInfo(format("Please open http://%s:%d/ in your browser.",
            conf["bind_ip"].get!string,
            conf["port"].get!short));
}

void gitlab_event(HTTPServerRequest req, HTTPServerResponse res)
{
    auto code = gl.handler(req.json);
    if (200 == code)
        res.writeBody("Ok");
    else {
        auto str = format("Error: %d\n", code);
        res.writeBody(str);
    }
}

void def(HTTPServerRequest req, HTTPServerResponse res)
{
    res.writeBody("API not specified");
}
